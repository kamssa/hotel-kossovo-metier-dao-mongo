package ci.kossovo.hotel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HotelKossovoMetierDaoMongoApplication {

	public static void main(String[] args) {
		SpringApplication.run(HotelKossovoMetierDaoMongoApplication.class, args);
	}
}
